package package_06_09_Threads;

public class RunnableAffiche {
	
	public static void main(String[] args) {
	    Runnable runnable = new Traitement();

	    for (int i = 0; i < 10; i++) {
	      Thread thread = new Thread(runnable);
	      thread.setName("monTraitement-" + i);
	      thread.start();
	    }
	  }

}
