package package_06_09.metier;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GestionMetierImpl  implements IGestionMetier{

	@Override
	public List<Etudiant> etudiantParMC(String mc) {
		
		List<Etudiant> etudiant = new ArrayList<Etudiant>();
		try {
			//charger un pilote driver
			Class.forName("com.mysql.jdbc.Driver");
			
			// créer un objet connection; pour établir une connection avce la base de donnée
		    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/gestion?autoReconnect=true&useSSL=false","root","issa");
			
		    //prépation de la réquete
		    PreparedStatement ps = conn.prepareStatement("select * from etudiant where nom like ?");
			ps.setString(1, "%"+mc+"%");
			
			//execute et récupération du resultat de la réquete
			ResultSet rs = ps.executeQuery();
			
			//afficher les resultat
			while(rs.next()) {
				Etudiant et = new Etudiant();
				et.setIdEtudiant(rs.getInt("id"));
				et.setNomEtudiant(rs.getString("nom"));
				et.setPrenomEtudiant(rs.getString("prenom"));
				et.setUnivEtudiant(rs.getString("universite"));
				etudiant.add(et);
				
			}
			// Ferméture de la connection
			ps.close();
			conn.close();
			
		}catch (Exception e) {
			 
			e.printStackTrace();
		}
		return etudiant;
	}

	@Override
	public void addEtudiant(Etudiant et) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/gestion?autoReconnect=true&useSSL=false","root","issa");
			PreparedStatement ps = conn.prepareStatement("insert into etudiant(nom, prenom, universite) value(?, ?, ?) ");
			ps.setString(1, et.getNomEtudiant());
			ps.setString(2, et.getPrenomEtudiant());
			ps.setString(3, et.getUnivEtudiant());
			int nb = ps.executeUpdate();
			ps.close();
			conn.close();
			
		} catch (Exception e) {
			 
			e.printStackTrace();
		}
		
	}

}
