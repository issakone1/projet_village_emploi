package package_06_09.metier;

import java.util.List;

public interface IGestionMetier {
	
	public List<Etudiant> etudiantParMC(String mc);
	public void addEtudiant(Etudiant e);

}
