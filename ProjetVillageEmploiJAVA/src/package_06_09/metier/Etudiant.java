package package_06_09.metier;

import java.io.Serializable;

public class Etudiant implements Serializable{
	
	private int idEtudiant;
	private String nomEtudiant;
	private String prenomEtudiant;
	private String univEtudiant;
	
	
	public int getIdEtudiant() {
		return idEtudiant;
	}




	public void setIdEtudiant(int idEtudiant) {
		this.idEtudiant = idEtudiant;
	}




	public String getNomEtudiant() {
		return nomEtudiant;
	}




	public void setNomEtudiant(String nomEtudiant) {
		this.nomEtudiant = nomEtudiant;
	}




	public String getPrenomEtudiant() {
		return prenomEtudiant;
	}




	public void setPrenomEtudiant(String prenomEtudiant) {
		this.prenomEtudiant = prenomEtudiant;
	}




	public String getUnivEtudiant() {
		return univEtudiant;
	}




	public void setUnivEtudiant(String univEtudiant) {
		this.univEtudiant = univEtudiant;
	}




	public Etudiant(String nomEtudiant, String prenomEtudiant, String univEtudiant) {
		super();
		
		this.nomEtudiant = nomEtudiant;
		this.prenomEtudiant = prenomEtudiant;
		this.univEtudiant = univEtudiant;
	}



	
	public Etudiant() {
		super();
	}
	
	
	

}
