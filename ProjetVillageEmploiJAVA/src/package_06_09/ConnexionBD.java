package package_06_09;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// cette classe n'est pas utilisée
public class ConnexionBD {
    
    Connection con;
    
    public ConnexionBD(){
        
      try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch(ClassNotFoundException e){
            System.err.println( e);
        }
        try{
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/gestion?autoReconnect=true&useSSL=false","root","issa");
        }catch(SQLException e){
            System.err.println( e);
        }
       
    }
   public Connection obtenirconnexion(){
        return con;
    }
    
   
    
}


