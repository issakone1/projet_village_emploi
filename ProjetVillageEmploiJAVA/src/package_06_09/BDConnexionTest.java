package package_06_09;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

//cette classe n'est pas utilisée, mais vous pouvez tester
public class BDConnexionTest {
	
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/gestion","root","issa");
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("select * from etudiant");
			
			/*while (rs.next()) {
				System.out.println(rs.getString("id"));
				System.out.println(rs.getString("nom"));
				System.out.println(rs.getString("prenom"));
				System.out.println(rs.getString("universite"));
				
			}*/
			
			//si on ne connait pas la structure de la table
			
			ResultSetMetaData rsmd = rs.getMetaData();
			for (int i = 1; i <= rsmd.getColumnCount(); i++) {
				System.out.print(rsmd.getColumnName(i)+"\t");
			}
			System.out.println();
			while(rs.next()) {
				for (int j = 1; j <= rsmd.getColumnCount(); j++) {
					System.out.print(rs.getString(j)+"\t");	
				}
				System.out.println();
			}
		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
