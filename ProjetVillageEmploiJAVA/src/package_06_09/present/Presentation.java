package package_06_09.present;

import java.util.List;
import java.util.Scanner;

import package_06_09.metier.Etudiant;
import package_06_09.metier.GestionMetierImpl;

public class Presentation {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		GestionMetierImpl metier = new GestionMetierImpl();
		
		System.out.println("Nom Etudiant :"); String nom = sc.next();
		System.out.println("Prenom Etudiant :"); String prenom = sc.next();
		System.out.println("Université :"); String univ = sc.next();
		Etudiant etude = new Etudiant(nom, prenom, univ);
		metier.addEtudiant(etude);
		
		
		System.out.println("Entré le mot clé :");
		String mc = sc.next();
		List<Etudiant> etud = metier.etudiantParMC(mc);
		for(Etudiant et:etud) {
			System.out.println(et.getIdEtudiant()+"\t"+et.getNomEtudiant()+"\t"
					+et.getPrenomEtudiant()+"\t"+et.getUnivEtudiant());
		}	
		
	}

}
