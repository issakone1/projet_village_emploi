package package_03_09;

public class Ville12 {
	
	protected String nom;
	 private int nombrehabitant;
	//constructeur par défaut explicite
	public Ville12(){
		this.nom=null;
		this.nombrehabitant=0;
	}
	
	public Ville12(String nom, int nombrehabitant){
		this.nom=nom;
		this.nombrehabitant=nombrehabitant;
	}
	
	
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNombrehabitant() {
		return nombrehabitant;
	}

	public void setNombrehabitant(int nombrehabitant) {
		this.nombrehabitant = nombrehabitant;
	}

	public String toSting(){
		
		return "Le nombre d'habitant de "+getNom()+" est "+getNombrehabitant();
		
	}
	
    /*public String toString(){
		
		return "Le nombre d'habitant de "+this.nom+" est "+this.nombrehabitant;
		
	}*/

}
