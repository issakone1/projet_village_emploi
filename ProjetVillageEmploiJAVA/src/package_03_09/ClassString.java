package package_03_09;

import java.util.Locale;

public class ClassString {

	public static void main(String[] args) {
		// on a crée un objet de type string
		String s1 = new String("Mon cahier de charge");
		String s2 = " Un peu de juit de fruit";
		String s3 = "Bonbon";
		String s4 = "bonbon";
		//Locale local=new Locale("CANADA");
		
		//System.out.println("a1 = "+ s1.replace(String.valueOf(855456).substring(0, 1), 'm'));
		System.out.println("a2 ="+ s1.toLowerCase());
		System.out.println("a3 ="+ s1.toUpperCase());
		System.out.println("a4 ="+ s1.startsWith("M"));
		System.out.println("a5 ="+ s1.startsWith("Mon",0));
		System.out.println("a6 ="+ s1.endsWith("charge"));
		
		
//		System.out.println("s27 ="+ s1.concat(s2));
//		System.out.println("s28 ="+ s1.contains("Mon"));
//		System.out.println("s29 ="+ s1.contains("ton"));
//		System.out.println("s30 ="+ "kan".contentEquals("kan"));
//		System.out.println("s31 ="+ "kin".contentEquals("kan"));
		
		

		//System.out.println("s1 = " + s1);
		 //s1.substring(0,1 );
		/*
		 * La math
		 * 
		 */
//		System.out.println("s2 ="+ s1);
//		System.out.println("s3 = " + s1.substring(0, 1));
//		System.out.println("s4 = " + s1.replace(s1.substring(0, 1), "m"));
//		System.out.println("s5 = " + s1.hashCode());
//		System.out.println("s6 = " + s1.isEmpty());
//		System.out.println("s7 = " + s1.charAt(1));
//		System.out.println("s8 = " + s1.isEmpty());
//		System.out.println("s9 = " + s1.codePointAt(0));
//		System.out.println("s10 = " + s1.codePointBefore(1));
//		System.out.println("s11 = " + s1.codePointCount(1, 5));
//		System.out.println("s12 = " + s1.compareTo(s2));
//		System.out.println("s13 = " + s1.replaceAll(s1, s2));
//		System.out.println("s14 = " + s1.replaceAll("de", "of"));
//		System.out.println("s15 = " + s1.replaceFirst("Mon", "iskjnbuh"));
//		System.out.println("s16 = " + s1.replaceFirst(s1, s2));
//		System.out.println("s17 = " + s3.compareTo(s4));
//		System.out.println("s18 = " + s3.compareToIgnoreCase(s4));
//		System.out.println("s19 = " + s4.compareToIgnoreCase(s3));
//		System.out.println("s20 = " + s4.compareTo(s3));
//		System.out.println("s21 = " + s4.equals(s3));

//		s3 = s4;
//		System.out.println("s22 = " + s4 == s3);
//		System.out.println("s23 = " + s4.compareTo(s3));
//		System.out.println("s24 = " + s4.equals(s3));
//		System.out.println("s25 = " + s4.compareTo(s3));
//		System.out.println("s26 = " + s3);

	}

}
