package package_03_09;

import java.awt.Point;
import java.util.Vector;

public class ClasseVector {

	public static void main(String[] args) {
		
		String s1 = new String("Mon cahier de charge");
		String s2 = new String("In my mine");
		Vector v1=new Vector();
		Vector v2=new Vector();
		v1.addElement(s1);
		v1.addElement(v1);
		v1.addElement(v2);
		v1.addElement(s2);
		v1.addElement(2000);
		v1.addElement(new Point(4, 4));
		System.out.println("v1 = "+v1);
		System.out.println("v2 = "+v1.isEmpty());
		//System.out.println(v1.remove(4));
		System.out.println("v3 = "+v1.size());
		System.out.println("v4 = "+v1.capacity());
		System.out.println("v5 = "+v1.clone());
		System.out.println("v6 = "+v1.firstElement());
		System.out.println("v7 = "+v1.get(4).getClass().getSimpleName());
		//System.out.println("v8 = "+v1.iterator());
//		v1.remove(v1);
//		System.out.println("v8 = "+v1);
//		System.out.println("v9 = "+v1.remove(s1));
		System.out.println("v10 = "+v1.contains(s1));
		//System.out.println("v11 = "+v1.elements());
		try{
        for(int i=0; i<=v1.size();i++){
        		System.out.println("v12 = "+v1.get(i));
        	 }
		}catch(ArrayIndexOutOfBoundsException e){
			
		}

	}

}
