package package_03_09;

public class Personne11 {
	
	private String nom;
	private String prenom;
	private int année;
	private String conjoint;
	
	
	public  Personne11(String prenom, String nom, int année, String conjoint){
		this.nom=nom;
		this.prenom=prenom;
		this.année=année;
		this.conjoint=conjoint;
		
	}
	
	public Personne11(String prenom, String nom, int année){
		this.nom=nom;
		this.prenom=prenom;
		this.année=année;
	}
	
	public String getNom(){
		return nom;
	}
	
	public String getPrenom(){
		return prenom;
	}
	
	public int getAnnee(){
		return année;
	}
	
	public String getConjoint(){
		return conjoint;
	}
	
	public void setNom(String nom){
		this.nom=nom;
	}
	
	public void setPrenom(String prenom){
		this.prenom=prenom;
	}
	
	public void setAnnee(int année){
		this.année=année;
	}
	
	public void setConjoint(String conjoint){
		this.conjoint=conjoint;
	}
	
	public  String toString(){
		return this.nom+" "+this.prenom+" est n� en "+this.année+", il(elle) est "+this.conjoint;
		
	}
	
	public static int Age(int anne){
		return 2017 - anne;
		
	}

}
