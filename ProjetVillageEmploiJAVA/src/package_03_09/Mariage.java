package package_03_09;

public class Mariage {
	
	//String civilite;
	char sexe;
	String nom;
	String prenom;
	String anneeNais;
	String situationMatrimoniale;
	public Mariage(String nom, String prenom, String anneeNais, char sexe ,String situationMatrimoniale) {
		super();
		//this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.anneeNais = anneeNais;
		this.sexe=sexe;
		this.situationMatrimoniale = situationMatrimoniale;
	}
	
	public String retourneInfo(){
		String res=null;
		if(sexe=='M'){
			res= "M."+" "+this.prenom+" "+this.nom+" est née "+this.anneeNais+" il est "+this.situationMatrimoniale;
		}
		
		if(sexe=='F'){
			if(situationMatrimoniale=="célibataire"){
			res= "Mlle"+" "+this.prenom+" "+this.nom+" est née "+this.anneeNais+" elle est "+this.situationMatrimoniale;
		}
			if(situationMatrimoniale=="mariée"){
				res= "Mme"+" "+this.prenom+" "+this.nom+" est née "+this.anneeNais+" elle est "+this.situationMatrimoniale;
	}
		
		}
		
		return res;
		
	}

}
