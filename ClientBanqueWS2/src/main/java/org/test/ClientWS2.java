package org.test;

import java.util.List;

import org.ws.BanqueService;
import org.ws.BanqueWS;
import org.ws.Compte;

public class ClientWS2 {

	public static void main(String[] args) {
		
		BanqueService proxy = new BanqueWS().getBanqueServicePort();
		
		System.out.println(proxy.conversionEuroToCfa(1)+"\n");
		
		Compte compte = proxy.getCompte(3);
		System.out.println("code = "+compte.getCode());
		System.out.println("solde = "+compte.getSolde());
		System.out.println("===============");
		
		List<Compte> cptes  = proxy.listComptes();
		for (Compte cp : cptes) {
			System.out.println("===============");
			System.out.println("code = "+cp.getCode());
			System.out.println("solde = "+cp.getSolde());
		}
	}

}
