float nombre_de_part(int nbrenfant);
/*Cette fonction prend en entrer un entier et retourne un flottant.
Elle permet de calculer le nombre de parts du salari� qui permet de savoir
 la r�duction qu'on va lui accorder � la fin du  calcul de l'impot.*/

int brutfisan(int salaire );
/*Cette fonction prend en entrer un entier et returne un en entier.
Elle permet de calculer le brut fiscal annuel*/

float abattements(int bfa);
/*Cette fonction prend en entrer un entier et return un flottant.
Elle permet de calculer l'abattement.*/

int bfapresab(int bfa,int abattement);
/*Cette fonction prend en entrer deux entiers et return un entier.
Elle permet de calculer le  brut fiscal annuel apr�s l'abattement.*/

int impotrpp(int pot);
/*Cette fonction prend en entrer un entier et retourne un entier.
Elle permet de calculer l'impot sur le revenu des personnes physiques.*/

int reduction(float d,int p);
/*Cette fonction prend en entrer un entier et un flottant et retourne un entier.
Elle permet de calculer de la r�duction � appliquer sur l�IRPP.*/

int impotfinal(int pi,int re);
/*Cette fonction prend en entrer un entier et un entier et retourne un entier.
Elle permet de calculer l'impot final.*/


