//#ifndef GF_H_INCLUDED
//#define GF_H_INCLUDED
////36
//#include <inttypes.h>
//#define MAX_EXT_DEG 16
//typedef uint16_t gf_t;
////39
//#define gf_unit( ) 1
//#define gf_zero( ) 0
//#define gf_add(x, y) ((x)^(y))
//#define _gf_modq_1 (d)(((d) & gf_ord) + ((d) >> gf_extd))
//#define gf_mul_fast(x, y) ((y) ? gf_exp[_gf_modq_1(gf_log[x] + gf_log[y])] : 0)
//#definegf_mul(x, y) ((x) ? gf_mul_fast(x, y) : 0)
//#define gf_square(x)((x) ?gf_exp[_gf_modq_1(gf_log[x] << 1)] : 0)
//#define gf_sqrt(x)((x) ? gf_exp[_gf_modq_1(gf_log[x] << (gf_extd−1))] : 0)
//#define gf_div(x, y) ((x) ? gf_exp[_gf_modq_1(gf_log[x]−gf_log[y])] : 0)
//#define gf_inv(x)gf_exp[gf_ord−gf_log[x]]
////37
//extern void gf_init(int extdeg);
//extern void gf_free( );
//extern gf_t gf_pow(gf_t x,int i);
//extern gf_t gf_rand(int(*u8rnd)( ));
////38
//extern int gf_extd;
//extern int gf_card;
//extern int gf_ord;
//extern gf_t *gf_log;
//extern gf_t *gf_exp;
//
//#endif




/*int *gf_antilog;
int *gf_log;
int *gf_pol_primitif;

int listPol(int m);
int multiplication(int a,int b);
int inverse(int a);
int puissance(int a,int k);
int antiLog(int b);
int loge(int a);*/
