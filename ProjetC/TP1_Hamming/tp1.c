/*
 * info-528,
 * entêtes pour le tp1
 */




#include "tp1.h"


/*
 * Affiche un octet en binaire.
 */

/*void print_octet(octet u) {
    int t;
    for(i=0;i<=7;i++){
       printf("%u*,BIT(u,t));

}*/



/*
 * Calcule le nombre de bits différents entre les deux octets u et v
 */
int distance_hamming_octet (octet u, octet v) {
     int i,
       compt=0;
      for(i=0;i<=7;i++){
          if(BIT(u,i)!=BIT(v,i)){
             compt=compt+1;
          }
       }
       return compt;
 
}

/*
 * Calcule le nombre de bits différents entre les deux tableaux d'octets
 * u et v (tous les deux de taille "taille")
 */
int distance_hamming (const octet *u, const octet *v, int taille) {

    int i;
    int compt;
    int dist=0;
   
    for(i=0;i<taille;i++){
       compt=distance_hamming_octet ( u[i], v[i]);
       dist=compt + dist;
    }
    return dist;
}




/*
 * Encodage de Hamming pour un octet. Le résultat (2 octets) est mis dans
 * "sortie".
 */
void hamming_code_octet(octet u, octet sortie[2]) {

    //instruction
    //instruction
    //instruction
    
   
}



/*
 * Encodage un fichier en utilisant le code de Hamming
 */
int hamming_code(FILE *in, FILE *out) {

    //instruction
    //instruction
    //instruction
    
    //Ã  modifier
    return 1;
}


/*
 * Calcule le syndrome d'un mot de Hamming avec la matrice de parité.
 */
octet hamming_syndrome(octet u) {

    //instruction
    //instruction
    //instruction
    
    //Ã  modifier
    return 1;
}


/*
 * Calcule le masque d'erreur correspondant à un syndrome...
 * Si plus d'une erreur détectée, renvoie "0xff".
 */
octet hamming_erreur(octet s) {

    //instruction
    //instruction
    //instruction
    
    //Ã  modifier
    return 1;
}


/*
 * Décodage deux mots de Hamming en un seul octet.
 */
void hamming_decode_octet(const octet u[2], octet v[1]) {

    //instruction
    //instruction
    //instruction
    
    
}

/* Décodage (et correction) d'un fichier */
int hamming_decode(FILE *in, FILE *out) {

    //instruction
    //instruction
    //instruction
    
    //Ã  modifier
    return 1;
}



/*
 * Introduit du bruit dans "avant" et met le resultat dans "apres".
 * "p" représente la probabilité qu'un bit soit modifié.
 * Attention, il faut que "apres" soit suffisamment grand pour recevoir le
 * resultat...
 */
int bruite (const octet *avant, octet *apres, int taille, float p) {

    //instruction
    //instruction
    //instruction
    
    //Ã  modifier
    return 1;
}



