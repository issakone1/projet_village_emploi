/*
 * info-528,
 * entêtes pour le tp1
 */

#ifndef _TP1_H
#define _TP1_H

/* bibliothèques qui peuvent servir... */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <getopt.h>  /* gestion des arguments optionels de la ligne de commandes */
#include <time.h>    /* pour initialiser la graine pour générer des nombres aléatoires */



/*
 * Type des octets
 * (C'est mieux de prendre des non signés pour eviter les debordements, et
 * pour que les "shift" soient bien des shift logiques et non pas
 * arithmétiques.)
 */
typedef unsigned char octet ;


/*
 * Question 1
 * Écrivez une macro 
 * #define BIT(u,i)  ...
 * qui permettra d'obtenir facilement un bit dans un octets.
 */
        #define BIT(u,i)            \
        (u>>(7-i))&(1) 




/*
 * Affiche un octet en binaire.
 */
void print_octet(octet u) ;

/*
 * Calcule le nombre de bits différents entre les deux octets u et v
 */
int distance_hamming_octet (octet u, octet v) ;

/*
 * Calcule le nombre de bits différents entre les deux tableaux d'octets
 * u et v (tous les deux de taille "taille")
 */
int distance_hamming (const octet *u, const octet *v, int taille) ;




/*
 * Encodage de Hamming pour un octet. Le résultat (2 octets) est mis dans
 * "sortie".
 */
void hamming_code_octet(octet u, octet sortie[2]) ;

/*
 * Encodage un fichier en utilisant le code de Hamming
 */
int hamming_code(FILE *in, FILE *out) ;

/*
 * Calcule le syndrome d'un mot de Hamming avec la matrice de parité.
 */
octet hamming_syndrome(octet u) ;

/*
 * Calcule le masque d'erreur correspondant à un syndrome...
 * Si plus d'une erreur détectée, renvoie "0xff".
 */
octet hamming_erreur(octet s) ;

/*
 * Décodage deux mots de Hamming en un seul octet.
 */
void hamming_decode_octet(const octet u[2], octet v[1]) ;

/* Décodage (et correction) d'un fichier */
int hamming_decode(FILE *in, FILE *out) ;



/*
 * Introduit du bruit dans "avant" et met le resultat dans "apres".
 * "p" représente la probabilité qu'un bit soit modifié.
 * Attention, il faut que "apres" soit suffisamment grand pour recevoir le
 * resultat...
 */
int bruite (const octet *avant, octet *apres, int taille, float p) ;


#endif  /* _TP1_H */
