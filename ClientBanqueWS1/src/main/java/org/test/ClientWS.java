package org.test;

import java.util.Iterator;

import org.ws.BanqueServiceProxy;
import org.ws.Compte;

public class ClientWS {

	public static void main(String[] args) throws Exception{
		BanqueServiceProxy proxy = new BanqueServiceProxy();
		System.out.println(proxy.conversionEuroToCfa(1)+"\n");
		
		Compte compte = proxy.getCompte(3);
		System.out.println("code = "+compte.getCode());
		System.out.println("solde = "+compte.getSolde());
		
		Compte[] cptes  = proxy.listComptes();
		for (Compte cp : cptes) {
			System.out.println("===============");
			System.out.println("code = "+cp.getCode());
			System.out.println("solde = "+cp.getSolde());
		}

	}

}
