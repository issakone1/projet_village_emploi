package org.sid.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
//@DiscriminatorValue("Tache ALC")
public class TacheAuLongCours extends Tache{
	
	private String avancement;

	public TacheAuLongCours() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TacheAuLongCours(String titre, Date dateDebut, Date dateFin, Categories categories, String avancement) {
		super(titre, dateDebut, dateFin, categories);
		this.avancement = avancement;
	}

	public String getAvancement() {
		return avancement;
	}

	public void setAvancement(String avancement) {
		this.avancement = avancement;
	}

	
	
	
}
