package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Categories implements Serializable {
	
	@Id @GeneratedValue
	private Long idCategorie;
	private String typeCategorie;
	
	@OneToMany(mappedBy = "categories", fetch = FetchType.LAZY)
	private Collection<Tache> tache;
	
	public Categories() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Categories(String typeCategorie) {
		super();
		this.typeCategorie = typeCategorie;
	}
	public Long getIdCategorie() {
		return idCategorie;
	}
	public void setIdCategorie(Long idCategorie) {
		this.idCategorie = idCategorie;
	}
	public String getTypeCategorie() {
		return typeCategorie;
	}
	public void setTypeCategorie(String typeCategorie) {
		this.typeCategorie = typeCategorie;
	}
	
	
	
	
}
