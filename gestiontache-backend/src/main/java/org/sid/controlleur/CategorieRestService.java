package org.sid.controlleur;

import java.util.List;

import org.sid.entities.Categories;
import org.sid.repository.CategoriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategorieRestService {
	@Autowired
	private CategoriesRepository categoriesRepository;
	
	/*@RequestMapping(value="/categorie",method = RequestMethod.GET)
	public Page<Categories> listCategorie(int page, int size){
		return categoriesRepository.findAll(PageRequest.of(page, size));
	}*/
	
	@RequestMapping(value="/categorie",method = RequestMethod.GET)
	public List<Categories> listCategorie(){
		return categoriesRepository.findAll();
	}
	
	@RequestMapping(value="/categorie/{id}",method = RequestMethod.GET)
	public Categories getCategorie(@PathVariable("id") Long id){
		return categoriesRepository.findById(id).orElse(null);
	}
	
	@RequestMapping(value="/categorie",method = RequestMethod.POST)
	public Categories ajouter(@RequestBody Categories t) {
		return categoriesRepository.save(t);
	}
	
	@RequestMapping(value="/categorie/{id}",method = RequestMethod.PUT)
	public Categories modifier(@RequestBody Categories t, @PathVariable Long id) {
		t.setIdCategorie(id);
		return categoriesRepository.saveAndFlush(t);
	}
	
	@RequestMapping(value="/categorie/{id}",method = RequestMethod.DELETE)
	public void supprimer(@PathVariable("id") Long id){
		categoriesRepository.deleteById(id);
	}
}
