package org.sid.controlleur;

import java.util.List;

import javax.websocket.server.PathParam;

import org.sid.entities.Categories;
import org.sid.entities.Tache;
import org.sid.repository.TacheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
//@RequestMapping("/")
public class TacheRestService {
	
	@Autowired
	private TacheRepository tacheRepository;
	
	@RequestMapping(value="/tache",method = RequestMethod.GET)
	public Page<Tache> listTache(int page, int size){
		return tacheRepository.findAll(PageRequest.of(page, size));
	}
	
	/*@GetMapping(value="tache")
	public List<Tache> listTache(){
		return tacheRepository.findAll();
	}*/
	
	@RequestMapping(value="/tache/{id}",method = RequestMethod.GET)
	public Tache getTache(@PathVariable("id") Long id){
		return tacheRepository.findById(id).orElse(null);
	}
	
	@RequestMapping(value="/tache",method = RequestMethod.POST)
	public Tache ajouter(@RequestBody Tache t) {
		return tacheRepository.save(t);
	}
	
	@RequestMapping(value="/tache/{id}",method = RequestMethod.PUT)
	public Tache modifier(@RequestBody Tache t, @PathVariable Long id) {
		t.setIdTache(id);
		return tacheRepository.saveAndFlush(t);
	}
	
	@RequestMapping(value="/tache/{id}",method = RequestMethod.DELETE)
	public void supprimer(@PathVariable("id") Long id){
		tacheRepository.deleteById(id);
	}

}
