package org.sid;

import java.util.Date;
import java.util.List;

import org.sid.repository.*;
import org.sid.entities.Categories;
import org.sid.entities.Tache;
import org.sid.entities.TacheAuLongCours;
import org.sid.entities.TachePonctuelle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class GestiontacheBackendApplication {

	public static void main(String[] args) {
		//SpringApplication.run(GestiontacheBackendApplication.class, args);
		
		ApplicationContext ctx = SpringApplication.run(GestiontacheBackendApplication.class, args);
		
		
		  CategoriesRepository categoriesRepository = ctx.getBean(CategoriesRepository.class); 
		  Categories cat1 = categoriesRepository.save(new Categories("Travail"));
		  Categories cat2 = categoriesRepository.save(new Categories("Personnel"));
		  Categories cat3 = categoriesRepository.save(new Categories("Jeux"));
		  
		  
		 TacheRepository tacheRepository = ctx.getBean(TacheRepository.class); 
		 Tache t1 = tacheRepository.save(new TachePonctuelle("vacance", new Date(), new Date(), cat1));
		 Tache t2 = tacheRepository.save(new TacheAuLongCours("Training", new Date(), new Date(), cat2,"20%"));
		 Tache t3 = tacheRepository.save(new TacheAuLongCours("football", new Date(), new Date(), cat3,"80%"));
		 
		 
		//tacheRepository.deleteById((long) 12);
		 
		//t1.setIdTache((long) 4);
		//tacheRepository.saveAndFlush(t1);
		  
		List<Tache> tache = tacheRepository.findAll();
		tache.forEach(ta->System.out.println(ta.getIdTache()+" | "+ta.getTitre()+" | "+ta.getCategories().getTypeCategorie()+
				" | "+ta.getDateDebut()+" | "+ta.getDateFin()));
 
	}

}
