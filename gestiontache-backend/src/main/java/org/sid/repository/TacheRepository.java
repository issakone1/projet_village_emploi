package org.sid.repository;

import org.sid.entities.Tache;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface TacheRepository extends JpaRepository<Tache, Long> {
	
	//public Page<Tache> chercherProduits(@Param("x")String mc, Pageable pageable);
}
